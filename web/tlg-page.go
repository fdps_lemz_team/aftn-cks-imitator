package web

import (
	"fdps/utils/logger"
	"html/template"
	"sync"
)

type ConfigPage struct {
	sync.RWMutex
	editTempl *template.Template
	Title     string

}

func (cp *ConfigPage) initialize(title string) {
	cp.Lock()
	defer cp.Unlock()

	var err error
	if cp.editTempl, err = template.New("Tlg").Parse(ConfigTemplate); err != nil {
		logger.PrintfErr("EditConfig template Parse ERROR: %v", err)
		return
	}
	cp.Title = title
}

var ConfigTemplate = `
<title>{{.Title}}</title>
<h1></h1>

<form action="/send" method="POST">

<p>Комментарий<Br>
   <textarea name="TlgText" cols="40" rows="3"></textarea></p>
  <p><input type="submit" value="send" value="Отправить"></p>
</form>
<form action="/log" method="POST">
	<p><input type="submit" value="log" value="Журнал"></p>
</form>
`
