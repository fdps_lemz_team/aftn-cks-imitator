package web

import (
	"fmt"
	"net/http"
)

type EditTlgHandler struct {
	handleURL string
	title string
}

var EditTlgHndlr EditTlgHandler

func (cw EditTlgHandler) Path() string {
	return cw.handleURL
}

func (cw EditTlgHandler) Caption() string {
	return cw.title
}

func (ch EditTlgHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return editConfigHandler
}

func InitEditConfigHandler(handleURL string, title string) {
	EditTlgHndlr.handleURL = "/" + handleURL
	EditTlgHndlr.title = title
}

func editConfigHandler(w http.ResponseWriter, r *http.Request) {
	if err := srv.configPage.editTempl.ExecuteTemplate(w, "Tlg", srv.configPage); err != nil {
		fmt.Println("template ExecuteTemplate Tlg ERROR", err)
	}
}
