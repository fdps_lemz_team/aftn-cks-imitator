// web
package web

import (
	"context"
	"fdps/fdps-mons-ac/db"
	"fdps/fdps-mons-ac/fdps_mons"
	"fdps/utils"
	"fmt"
	"log"
	"net/http"
	"time"
)

type httpServer struct {
	http.Server
	shutdownReq   chan bool
	done          chan struct{}
	reqCount      uint32
	configPage    *ConfigPage
	monsSettsChan chan fdps_mons.ClientSettings
	dbSettsChan   chan db.DbSettings
}

var srv httpServer


func Start(done chan struct{}, netPort int) {
	srv = httpServer{
		Server: http.Server{
			Addr:         fmt.Sprintf(":%d", netPort),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
		//shutdownReq: sdc,
		done:       done,
		configPage: new(ConfigPage),
	}
	srv.configPage.initialize("IMITATE AFTN")

	InitEditConfigHandler(utils.AftnCcmImitTlgPath, "TLG")
	utils.AppendHandler(EditTlgHndlr)
	InitSaveConfigHandler(utils.AftnCcmImitSendPath, "SEND TLG")
	utils.AppendHandler(SendTlgHndlr)

	for _, h := range utils.HandlerList {
		http.HandleFunc(h.Path(), h.HttpHandler())
	}

	log.Printf("listening on %d port started", netPort)

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Printf("listen and serve error : %v", err)
		}
	}()
}

func (s *httpServer) stop() {
	log.Printf("stoping http server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	err := s.Shutdown(ctx)

	if err != nil {
		log.Printf("shutdown http server error: %v", err)
	}
}
