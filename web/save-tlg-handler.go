package web

import (
	"fdps/utils"
	"fdps/utils/logger"
	"net/http"
)

type SendTlgHandler struct {
	handleURL string
	title     string
}

var SendTlgHndlr SendTlgHandler
var TlgChan = make(chan string, 1)

func (sch SendTlgHandler) Path() string {
	return sch.handleURL
}

func (sch SendTlgHandler) Caption() string {
	return sch.title
}

func (sch SendTlgHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return saveConfigHandler
}

func InitSaveConfigHandler(handleURL string, title string) {
	SendTlgHndlr.handleURL = "/" + handleURL
	SendTlgHndlr.title = title
}

func saveConfigHandler(w http.ResponseWriter, r *http.Request) {

	tlgText := r.FormValue("TlgText")
	TlgChan <- tlgText
	logger.PrintfErr("TlgText %v", tlgText)

	http.Redirect(w, r, utils.AftnCcmImitTlgPath, http.StatusFound)
}
