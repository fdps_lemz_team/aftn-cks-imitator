package tcp_transport

import (
	"bytes"
	"net"
	"strconv"
	"sync"
	"time"

	"fdps/aftn/aftn-cks-imitator/settings"
	"fdps/aftn/aftn-pkg/channel/aftn"
	"fdps/aftn/aftn-pkg/channel/mtk2"
	"fdps/aftn/aftn-pkg/channel/tlg"
	"fdps/utils"
	"fdps/utils/logger"
)

const (
	acceptByte byte = 0x00 // байт подтверждения успешного подключения абонента
	rejectByte byte = 0x01 // байт разрыва соединения с абонентом

	escByte byte = 0xfd // байт управляющей последовательности
	lamByte byte = 0x01 // байт подтверждения получения сообщения

	blockByte   byte = 0xff // байт, означающий блокировку передачи сообщений
	unblockByte byte = 0xfe // байт, означающий восстановление передачи сообщений
)

var ImitSett settings.ImitateSettings

// ServerReadData
type ServerReadData struct {
	tcpAddr  string
	aftnAddr string
	readData []byte
}

// TcpClient отвечает за работу с TCPклиентом сервера
type TcpClient struct {
	sync.Mutex
	aftnAddr       []byte        // адрес абонента(СЗ866)
	aftnPing       byte          // значение интервала пинга
	pingInterval   time.Duration // значение интервала пинга (секунд)
	needPing       bool          // необходимость отправки пинг
	aftnSessionMsg []byte        // сообщение сессии(СЗ866)

	tcpConn        net.Conn
	cancelWorkChan chan struct{}
	errorChan      chan error

	readDataChan  chan []byte // канал для принятых данных
	readErrChan   chan error
	writeDataChan chan []byte // канал для отправки данных
	writeErrChan  chan error

	getClientAddr       bool // получен AFTN адрес абонента
	getClientSessionMsg bool // получено сообщение сессии

	readBuffer []byte

	parser       tlg.Parser
	curTlgNumber int // порядковый номер телеграммы
	//cfg          channel.Config

	// если байт управляющей последовательности был последним в буфере полученных байт,
	// то вырезаем его и при следующем получении вырезаем первый байт, если он равен байту Lam
	needCheckLam bool
	isBusy       bool // Нельзя посылать в канал (true) Причины 1 канал закрыт, 2 цкс прислал FF, 3 цкс не прислал FD01

	tlgChan chan []byte // канал для приема телеграм от telegrammSender
}

func newTcpClient(conn net.Conn) TcpClient {
	return TcpClient{
		tcpConn:        conn,
		cancelWorkChan: make(chan struct{}),
		errorChan:      make(chan error, 10),
		readDataChan:   make(chan []byte, 1024),
		readErrChan:    make(chan error, 1),
		writeDataChan:  make(chan []byte, 1024),
		writeErrChan:   make(chan error, 1),
		pingInterval:   time.Second * 10,
		needPing:       true,
		//cfg:            channel.GetConfig(),
		tlgChan: make(chan []byte, 10),
	}
}

// TCPServer серверное TCP подключение
type TCPServer struct {
	sync.Mutex

	ReceivedDataChan chan ServerReadData // канал для принятых данных
	SendDataChan     chan string         // канал для отправки данных

	tcpClients     map[*TcpClient]bool // клиентские подключения
	cancelWorkChan chan struct{}      // канал для сигнала прекращения отправки, чтения данных

	ClientConnectedChan    chan string
	ClientDisconnectedChan chan string

	ConnStateChan chan bool     // канал для передачи успешности подключения по TCP
	reconnectChan chan struct{} // канал для сообщения TCP клиенту о необходимости подключитья к серверу (не используется)

	errorChan chan error
}

// NewTcpServer конструктор
func NewTcpServer(setts settings.ImitateSettings) *TCPServer {
	ImitSett = setts

	return &TCPServer{
		ReceivedDataChan:       make(chan ServerReadData, 1024),
		SendDataChan:           make(chan string, 1024),
		ClientConnectedChan:    make(chan string, 10),
		ClientDisconnectedChan: make(chan string, 10),
		cancelWorkChan:         make(chan struct{}),
		ConnStateChan:          make(chan bool),
		errorChan:              make(chan error, 10),
		reconnectChan:          make(chan struct{}),
		tcpClients:             make(map[*TcpClient]bool, 0),
	}
}

// Work запуск работы контроллера
func (fts *TCPServer) Work(wg *sync.WaitGroup) {
	go fts.startServer()

	for {
		select {
		case data := <-fts.ReceivedDataChan:
			logger.PrintfInfo("Получены данные: <%s>. TCP адрес абонента: <%s>. AFTN адрес абонента: <%s>",
				data.readData, data.tcpAddr, data.aftnAddr)

		case <-fts.reconnectChan:

		case tlgFromWeb := <-fts.SendDataChan:
			for it, _ := range fts.tcpClients {
				var b bytes.Buffer

				regIdx := 0

				// заголовок телеграфного сообщения
				b.WriteString(string(aftn.REG[regIdx]))
				b.WriteString(aftn.ZCZC[regIdx])
				b.WriteString(" ")
				b.WriteString(ImitSett.OutName)
				nn := ImitSett.OutNum.Next().String()
				b.WriteString(nn)
				b.WriteString(" ")
				b.WriteString(aftn.TimeString()) //time.Now().UTC().Format(channel.ChannTimeFormat))
				b.WriteString("     ")
				b.WriteString(string(aftn.REG[regIdx]))
				b.WriteString(ImitSett.CrLf)
				b.WriteString(tlgFromWeb)
				// if !rt.IsChMessage() {
				// 	b.WriteString(string(aftn.REG[regIdx]))
				// }
				b.WriteString(ImitSett.CrLf)
				// if !rt.IsChMessage() {
				// 	b.WriteString(aftn.TelegramDelimiter)
				// }
				b.WriteString(aftn.NNNN[regIdx])
				b.WriteString(aftn.MessageDelimiter)

				//tlgChan <- mtk2.Mtk2{}.FromString(b.String())
				it.writeDataChan <- mtk2.Mtk2{}.FromString(b.String())
			}
		}
	}
	wg.Done()
}

//
func (fts *TCPServer) startServer() {
	var listener net.Listener
	listener, err := net.Listen("tcp", string(ImitSett.IPAddr+":"+strconv.Itoa(ImitSett.Port)))
	if err != nil {
		logger.PrintfErr("Ошибка запуска TCP сервера FMTP канала. Ошибка: <%s>", err.Error())

		fts.ConnStateChan <- false
		return
	} else {
		logger.PrintfInfo("Успешный запуск TCP сервера. tcp %s:%d", ImitSett.IPAddr, ImitSett.Port)

		fts.ConnStateChan <- true

		defer listener.Close()

		for {
			var curConn net.Conn
			curConn, err := listener.Accept()
			if err != nil {
				logger.PrintfErr("Ошибка подключения клиента к TCP серверу FMTP канала. Ошибка: <%s>", err.Error())
				continue
			}
			remoteAddr, _ := curConn.RemoteAddr().(*net.TCPAddr)

			//if fts.tcpClient != nil {
			// fts.errorChan <- common.LogChannelST(common.SeverityWarning,
			// 	fmt.Sprintf("Отклонено входящее подключение к TCP серверу FMTP канала. "+
			// 		"Клиент уже подключен. Адрес отклоненного клиента: <%s>", remoteAddr.IP.String()))
			// continue
			//} else {
			logger.PrintfInfo("Успешное подключение клиента к TCP серверу FMTP канала. Адрес подключенного клиента: <%s>", remoteAddr.IP.String())

			// fts.tcpClient = curConn
			// fts.connStateChan <- true
			// fts.fmtpEventChan <- fmtp.RSetup

			curClient := newTcpClient(curConn)
			fts.tcpClients[&curClient] = true
			go fts.handleConnection(curClient)
			//}
		}
	}
}

func (s *TCPServer) handleConnection(cl TcpClient) {
	go cl.receiveLoop()
	go cl.sendLoop()

	// таймер отправки пинга
	pingTimer := time.NewTimer(cl.pingInterval)
	pingTimer.Stop()

	// тикер для проверки работоспособности подключения
	liveCheckTimer := time.NewTimer(cl.pingInterval * 3)
	liveCheckTimer.Stop()

	// время получения каких-либо данных от ЦКС (для проверки работоспособности канала)
	lastDataCome := time.Now()

	// проверяем и запускаем, если необходимо таймер для отмены пингов
	if ImitSett.PingFailSeconds > 0 {
		time.AfterFunc(time.Second*time.Duration(ImitSett.PingFailSeconds), func() {
			cl.needPing = false
		})
	}

	// проверяем и запускаем, если необходимо таймер для сброса подключения
	if ImitSett.BreakConnSec > 0 {
		time.AfterFunc(time.Second*time.Duration(ImitSett.BreakConnSec), func() {
			if closeErr := cl.Close(); closeErr != nil {
				logger.PrintfErr("Ошибка сброса соединения по таймеру. Ошибка: %s.", closeErr)
			} else {
				delete(s.tcpClients, &cl)
				logger.PrintfWarn("Соединение сброшено по таймеру (см. config.json).")
			}
		})
	}

	// запускаем отправляльщиков телеграм
	for _, val := range ImitSett.Telegrams {
		go cl.telegrammSender(val, cl.tlgChan)
	}

	for {
		select {

		case readBytes := <-cl.readDataChan:
			lastDataCome = time.Now()

			logger.PrintfInfo("Считаны данные: <%s>.  TCP адрес: <%s>. AFTN адрес: <%s>.", utils.Cp866ToUtf8(readBytes),
				cl.tcpConn.RemoteAddr().String(), utils.Cp866ToUtf8(cl.aftnAddr))

			cl.readBuffer = append(cl.readBuffer, readBytes...)

			if !cl.getClientAddr && len(cl.readBuffer) >= aftn.AddressLen+aftn.PingLen {
				// считываем адрес абонента
				cl.aftnAddr = cl.readBuffer[:aftn.AddressLen]
				cl.readBuffer = cl.readBuffer[aftn.AddressLen:]
				// считываем значение пинга
				cl.aftnPing = cl.readBuffer[0]
				cl.pingInterval = time.Second * time.Duration(int(cl.aftnPing))
				cl.readBuffer = cl.readBuffer[aftn.PingLen:]
				cl.getClientAddr = true

				logger.PrintfInfo("Получено стартовое сообщение от абонента. Адрес: <%s>. Интервал Ping: <%d>",
					string(utils.Cp866ToUtf8(cl.aftnAddr)), int(cl.aftnPing))

				pingTimer.Reset(cl.pingInterval)

				liveCheckTimer.Reset(cl.pingInterval * 3)

				okFound := false
				for _, val := range ImitSett.OkClientAddr {
					if val == string(utils.Cp866ToUtf8(cl.aftnAddr)) {
						okFound = true
						break
					}
				}

				if okFound {
					logger.PrintfInfo("Адрес абонента удовлетворительный. Адрес: <%s>.", string(utils.Cp866ToUtf8(cl.aftnAddr)))

					cl.writeDataChan <- []byte{acceptByte}

					var addrMsg []byte
					addrMsg = append(addrMsg, utils.Utf8ToCp866([]byte(ImitSett.CksIdent))...)
					addrMsg = append(addrMsg, utils.Utf8ToCp866([]byte(ImitSett.CksAddr))...)
					cl.writeDataChan <- addrMsg
				} else {
					logger.PrintfErr("Адрес абонента неудовлетворительный. Адрес: <%s>.", string(utils.Cp866ToUtf8(cl.aftnAddr)))

					cl.writeDataChan <- []byte{rejectByte}
				}
			}

			if !cl.getClientSessionMsg && len(cl.readBuffer) >= aftn.AddressLen {
				copy(cl.aftnSessionMsg, cl.readBuffer)
				logger.PrintfInfo("Получено сообщение сессии. Сообщение: <%s>", mtk2.Mtk2{}.ToString(cl.aftnSessionMsg))
				cl.readBuffer = cl.readBuffer[:0]
				cl.getClientSessionMsg = true
			}

			if cl.getClientSessionMsg {

				if escInd := bytes.IndexByte(cl.readBuffer, escByte); escInd != -1 {
					logger.PrintfInfo("Получен байт управляющей последовательности.")

					if escInd < len(cl.readBuffer)-1 {
						if cl.readBuffer[escInd+1] == lamByte {
							logger.PrintfInfo("Получено подтверждение получения сообщения.")
							//cl.isBusy = false

							// удаляем байт управляющей последовательности и байт Lam
							readBytes = append(readBytes[:escInd], readBytes[escInd+2:]...)
						} else {
							// байт управляющей последовательности последний в буфере
							// удаляем его и при следующем полчении данных удаляем первый байт, если он обозначает Lam
							readBytes = readBytes[:escInd]
							//cl.needCheckLam = true
						}
					}
				}

				if !cl.isBusy {
					if ok, _, _, _ := cl.parser.Write(cl.readBuffer); ok {
						if rt, okTlg := cl.parser.GetTlg(); okTlg {
							var curDur time.Duration
							curDur, cl.curTlgNumber = ImitSett.GetBusyForTlgNum(cl.curTlgNumber)
							logger.PrintfWarn("Получена AFTN телеграмма: %+v. Number: %d. Duration: %v", rt,cl.curTlgNumber -1 , curDur)


							if curDur > time.Second*10 {
								cl.isBusy = true
								cl.writeDataChan <- []byte{escByte, blockByte}
								logger.PrintfDebug("Отправляем blockByte")
							}
							time.AfterFunc(curDur, func() {
								if cl.isBusy {
									cl.isBusy = false
									logger.PrintfDebug("Отправляем unblockByte")
									cl.writeDataChan <- []byte{escByte, unblockByte}
								}
								logger.PrintfErr("Отправляем Lam")
								cl.writeDataChan <- []byte{escByte, lamByte}
							})

						} else {
							logger.PrintfErr("Ошибка запроса AFTN телеграммы у парсера.")
						}
						cl.readBuffer = cl.readBuffer[:0]
					}

				}
			}

		case readErr := <-cl.readErrChan:
			logger.PrintfErr("Ошибка чтения данных от ЦКС. Ошибка: <%s>.", readErr)
			delete(s.tcpClients, &cl)
			_ = cl.Close()
			return

		// сработал таймер отправки PING
		case <-pingTimer.C:
			cl.writeDataChan <- []byte{0x00}
			logger.PrintfDebug("Отправлен пинг.")
			if cl.needPing {
				pingTimer.Reset(cl.pingInterval)
			} else {
				logger.PrintfWarn("Прекращаем отправлять PING (см. config.json).")
			}

		// проверка работоспособности подключения к ЦКС
		case <-liveCheckTimer.C:
			if time.Now().Add(cl.pingInterval * -3).After(lastDataCome) {
				logger.PrintfErr("За интервал времени, разный 3 * PING не получено данных. Соединение с ЦКС будет закрыто.")
				delete(s.tcpClients, &cl)
				_ = cl.Close()
				return
			} else {
				liveCheckTimer.Reset(cl.pingInterval * 3)
			}

			// отправщик телеграмм прислал данные для отправки
		case curData := <-cl.tlgChan:
			//if !cl.isBusy {
				cl.writeDataChan <- curData
			//} else {
			//	logger.PrintfWarn("Телеграмма не будет отправлена, т.к. канал занят.")
			//}
		}
	}
}

// Close закрыть подключение
func (cl *TcpClient) Close() error {
	cl.Lock()
	defer cl.Unlock()

	if isClosed := utils.ChanSafeClose(cl.cancelWorkChan); isClosed {
		if err := cl.tcpConn.Close(); err != nil {
			logger.PrintfErr("Ошибка при закрытии клиентского TCP подключения. Ошибка: <%s>.", err)
			return err
		} else {
			logger.PrintfWarn("Закрыто клиентское TCP подключение. TCP адрес: <%s>. AFTN адрес: <%s>.",
				cl.tcpConn.RemoteAddr().String(), string(utils.Cp866ToUtf8(cl.aftnAddr)))
		}
	}

	return nil
}

// обработчик получения данных
func (cl *TcpClient) receiveLoop() {
	for {
		select {
		// отмена приема данных
		case <-cl.cancelWorkChan:
			return
		// прием данных
		default:
			buffer := make([]byte, 8192)
			if readBytes, err := cl.tcpConn.Read(buffer); err != nil {
				logger.PrintfErr("Ошибка чтения данных из FMTP канала. Ошибка: <%s>.", err.Error())
				cl.readErrChan <- err
				return
			} else {
				logger.PrintfDebug("Приняты данные. Размер: %d байт. Данные: <%s>", readBytes, utils.Cp866ToUtf8(buffer[:readBytes]))
				cl.readDataChan <- buffer[:readBytes]
			}
		}
	}
}

// обработчик отправки данных
func (cl *TcpClient) sendLoop() {
	for {
		select {
		// отмена отправки данных
		case <-cl.cancelWorkChan:
			return

		// получены данные для отправки
		case curData := <-cl.writeDataChan:
			if count, err := cl.tcpConn.Write(curData); err != nil {
				logger.PrintfErr("Ошибка отправки данных клиенту. Ошибка: <%s>.", err.Error())
				cl.writeErrChan <- err
				return
			} else {
				logger.PrintfInfo("Отправка данных клиенту. Размер: %d байт. Данные: <%s>.", count, mtk2.Mtk2{}.ToString(curData))
			}
		}
	}
}

// обработчик отправки телеграмм
// принимает текст телеграммы и интервал отправки, по такмеру отправляет в канал tlgChan телеграмму в mtk2
func (cl *TcpClient) telegrammSender(curTlgInt settings.TlgInt, tlgChan chan []byte) {
	tlgTicker := time.NewTicker(time.Second * time.Duration(curTlgInt.Interval))

	for {
		select {
		// отмена отправки данных
		case <-cl.cancelWorkChan:
			return

		// сработал тикер отправки телеграммы
		case <-tlgTicker.C:
			var b bytes.Buffer

			regIdx := 0

			// заголовок телеграфного сообщения
			b.WriteString(string(aftn.REG[regIdx]))
			b.WriteString(aftn.ZCZC[regIdx])
			b.WriteString(" ")
			b.WriteString(ImitSett.OutName)
			nn := ImitSett.OutNum.Next().String()
			b.WriteString(nn)
			b.WriteString(" ")
			b.WriteString(aftn.TimeString()) //time.Now().UTC().Format(channel.ChannTimeFormat))
			b.WriteString("     ")
			b.WriteString(string(aftn.REG[regIdx]))
			b.WriteString(ImitSett.CrLf)
			b.WriteString(curTlgInt.Text)
			// if !rt.IsChMessage() {
			// 	b.WriteString(string(aftn.REG[regIdx]))
			// }
			b.WriteString(ImitSett.CrLf)
			// if !rt.IsChMessage() {
			// 	b.WriteString(aftn.TelegramDelimiter)
			// }
			b.WriteString(aftn.NNNN[regIdx])
			b.WriteString(aftn.MessageDelimiter)

			tlgChan <- mtk2.Mtk2{}.FromString(b.String())
		}
	}
}
