package settings

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"time"

	"fdps/aftn/aftn-pkg/channel"
	"fdps/utils"
)

// TlgNumDur номер телеграммы по счету и продолжительность ее обработки
type TlgNumDur struct {
	TlgNum   int `json:"TlgNum"`      // время возниконовения события (секунд от момента подключения клиента)
	Duration int `json:"DurationSec"` // продолжительность события (секунд)
}

// TlgInt текст телеграммы и интервал отправки
type TlgInt struct {
	Text     string `json:"Text"`     // текст телеграммы
	Interval int    `json:"Interval"` // интервал отправки (секунд)
}

// ImitateSettings настройки имитатора
type ImitateSettings struct {
	IPAddr           string        `json:"IPAddr"`           // IP адрес контроллера
	Port             int           `json:"Port"`             // идентификатор контроллера
	WebPort          int           `json:"WebPort"`          // порт web странички для отлпадки
	CksIdent         string        `json:"CksIdent"`         // телеграфный идентификатор ЦКС
	CksAddr          string        `json:"CksAddr"`          // AFTN адрес ЦКС
	OkClientAddr     []string      `json:"OkClientAddr"`     // адреса абонентов, которым будет одобрено подключение
	FailClientAddr   []string      `json:"FailClientAddr"`   // адреса абонентов, которым будет отказано в подключении
	PingFailSeconds  int           `json:"PingFailSec"`      //кол-во секунд от начала подключения, когда пропадет пинг
	BreakConnSec     int           `json:"BreakConnSec"`     // кол-во секунд от начала подключения, когда соединение будет сброшено
	DefaultBusySec   int           `json:"DefaultBusySec"`   // время обработки телеграммы по умолчанию (сек.). После получения через DefaultBusySec будет отправлен Lam
	BusyDurations    []TlgNumDur   `json:"BusyDurations"`    // список времен обработки телеграмм с определнным номером
	BusyMaxTlgNumber int           `json:"BusyMaxTlgNumber"` // max номер полученной телеграммы, после которой номер будт сброшен в 0
	Telegrams        []TlgInt      `json:"Telegrams"`        // телеграммы для отправки
	OutNum           channel.ChNum `json:"OutNum"`           // последний отправленный канальный номер
	OutName          string        `json:"OutNameRus"`       // обозначение канала на передачу (рус)
	CrLf             string        `json:"CrLf"`             // символы конца строки  (\r\n, \r\r\n)
}

// GetBusyForTlgNum время обработки для телеграммы с указанным номером
// возвращает длительность и номер следующей телеграммы
func (s ImitateSettings) GetBusyForTlgNum(tlgNum int) (time.Duration, int) {
	retValue := time.Duration(s.DefaultBusySec) * time.Second
	retNum := 0

	for _, val := range s.BusyDurations {
		if val.TlgNum == tlgNum {
			retValue = time.Duration(val.Duration) * time.Second
			break
		}
	}

	if tlgNum == s.BusyMaxTlgNumber {
		retNum = 0
	} else {
		retNum = tlgNum + 1
	}

	return retValue, retNum
}

var configFile = utils.AppPath() + "/config/config.json"

// ReadFromFile чтение ранее сохраненных настроек из файла
func (s *ImitateSettings) ReadFromFile() error {
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(data, &s); err != nil {
		fmt.Println(err.Error())
		return err
	}
	sort.Slice(s.BusyDurations, func(i, j int) bool {
		return s.BusyDurations[i].TlgNum < s.BusyDurations[j].TlgNum
	})
	return nil
}

// SaveToFile сохранение настроек в файл
func (s *ImitateSettings) SaveToFile() error {
	confData, err := json.Marshal(s)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(configFile, utils.JsonPrettyPrint(confData), os.ModePerm); err != nil {
		return err
	}
	return nil
}
