package main

import (
	"fmt"
	"sync"

	"fdps/aftn/aftn-cks-imitator/settings"
	"fdps/aftn/aftn-cks-imitator/tcp_transport"
	"fdps/aftn/aftn-cks-imitator/web"
	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/logger/log_ljack"
	"fdps/utils/logger/log_std"
	"fdps/utils/logger/log_web"
)

const appName = "imitate-aftn"

func initLoggers() {
	// логгер с web страничкой
	log_web.Initialize(log_web.LogWebSettings{
		StartHttp:    false,
		LogURLPath:   utils.AftnCcmImitLogPath,
		Title:        "Imitate AFTN",
		ShowSetts:    true,
		SettsURLPath: utils.AftnCcmImitTlgPath,
	})
	logger.AppendLogger(log_web.WebLogger)
	utils.AppendHandler(log_web.WebLogger)
	log_web.SetVersion("2020-01-29 13:35")

	if DockerVersion, dockErr := utils.GetDockerVersion(); dockErr != nil {
		log_web.SetDockerVersion("???")
		logger.PrintfErr(dockErr.Error())
	} else {
		log_web.SetDockerVersion(DockerVersion)
	}

	// логгер с сохранением в файлы
	var ljackSetts log_ljack.LjackSettings
	ljackSetts.GenDefault()
	ljackSetts.FilesName = appName + ".log"

	if err := log_ljack.Initialize(ljackSetts); err != nil {
		logger.PrintfErr(fmt.Sprintf("Ошибка инициализации логгера lumberjack. Ошибка: %s", err.Error()))
	}
	logger.AppendLogger(log_ljack.LogLjack)

	logger.AppendLogger(log_std.LogStd)

}

func main() {
	var setts settings.ImitateSettings
	var tcpServer *tcp_transport.TCPServer

	var wg sync.WaitGroup

	if readErr := setts.ReadFromFile(); readErr == nil {
		initLoggers()

		tcpServer = tcp_transport.NewTcpServer(setts)
		wg.Add(1)
		go tcpServer.Work(&wg)

	} else {
		logger.PrintfErr("Ошибка чтения настроек из файла. Ошибка: <%s>", readErr.Error())
		return
	}

	done := make(chan struct{})
	web.Start(done, setts.WebPort)

	for {
		select {
		case isConn := <-tcpServer.ConnStateChan:
			fmt.Println(isConn)

		case data := <-web.TlgChan:
			tcpServer.SendDataChan <- data
		}
	}

	wg.Wait()
}
